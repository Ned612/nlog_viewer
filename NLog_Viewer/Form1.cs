﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace NLog_Viewer
{
    public partial class Form1 : Form
    {
        DataTable dtData = new DataTable();
        
        public Form1()
        {
            InitializeComponent();

            //Add Data Columns
            dtData.Columns.Add("TimeStamp", typeof(string));
            dtData.Columns.Add("Level", typeof(string));
            dtData.Columns.Add("Category", typeof(string));
            dtData.Columns.Add("Message", typeof(string));
        }

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            foreach (string line in File.ReadLines(tbFilePath.Text))
            {
                string[] splitLine = line.Split('|');
                try
                {
                    string timeStamp = splitLine[0];
                    string level = splitLine[1];
                    string category = splitLine[2];
                    string message = splitLine[3];

                    dtData.Rows.Add(new object[] { timeStamp, level, category, message });

                    CreateLevelFilterList(level);
                    CreateCategoryFilterList(category);
                }
                catch (Exception)
                {

                }
            }

            //Applies Data to grid
            dataTable.DataSource = dtData;
            //dataTable.AutoResizeColumns();
            //dataTable.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            ColorCodeDataGrid();
        }

        void ColorCodeDataGrid()
        {
            foreach (DataGridViewRow row in dataTable.Rows)
            {
                switch (row.Cells[1].Value)
                {
                    case "Info":
                        row.DefaultCellStyle.BackColor = Color.White;
                        break;
                    case "Debug":
                        row.DefaultCellStyle.BackColor = Color.Yellow;
                        break;
                    case "Warning":
                        row.DefaultCellStyle.BackColor = Color.OrangeRed;
                        break;
                    case "Error":
                        row.DefaultCellStyle.BackColor = Color.OrangeRed;
                        break;
                    case "Trace":
                        row.DefaultCellStyle.BackColor = Color.Gray;
                        break;
                    case "Fatal":
                        row.DefaultCellStyle.BackColor = Color.Red;
                        break;
                    default:
                        break;
                }
            }
        }

        //TODO: Add count numbers
        void CreateLevelFilterList(string newLevel)
        {
            //Check if Unique
            foreach (string existingLevel in clbLevel.Items)
            {
                if (existingLevel == newLevel)
                    return;
            }

            //Is Unique
            clbLevel.Items.Add(newLevel, CheckState.Checked);
        }

        //TODO: Add count numbers
        void CreateCategoryFilterList(string newCategory)
        {
            //Check if Unique
            foreach (string existingCategory in clbCategories.Items)
            {
                if (existingCategory == newCategory)
                    return;
            }

            //Is Unique
            clbCategories.Items.Add(newCategory, CheckState.Checked);
        }

        //Filter Expressions: https://msdn.microsoft.com/en-us/library/cabd21yw(v=VS.100).aspx
        private void btnFilter_Click(object sender, EventArgs e)
        {
            string filterString = string.Empty;

            //Level Filtering
            foreach (string checkedItem in clbLevel.CheckedItems)
            {
                filterString += $"Level = '{checkedItem}'";
                filterString += " or ";
            }
            if (filterString.Length > 0) //Protects agaist no checkboxs selected
            {
                filterString = filterString.Remove(filterString.Length - 4); //Remove last ' or '
                dtData.DefaultView.RowFilter = filterString;
            }

            //TODO: Add Category Filtering
            //TODO: Add timestamp filtering / range

            ColorCodeDataGrid();
        }

        private void clbLevel_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            string filterString = string.Empty;

            
        }
    }
}
